extern crate clap;

#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(pub propositional_logic); // synthesized by LALRPOP
pub mod formula;

use clap::{Arg, App};

fn main() {
    let matches = App::new("X-Logics solver")
        .version("0.1")
        .author("Pierre Gillet")
        .about("A solver for the nonmonotonic logic called \"X-Logics\"")
        .arg(Arg::with_name("INPUT")
            .help("Sets the input file to use")
            .required(true)
            .index(1))
        .get_matches();


    use std::fs::File;
    use std::io::{prelude::*, BufReader};
    use propositional_logic::formulaParser;
    use propositional_solver::{SolverTree, Translation};

    // Calling .unwrap() is safe here because "INPUT" is required (if "INPUT" wasn't
    // required we could have used an 'if let' to conditionally get the value)
    let filename = matches.value_of("INPUT").unwrap();
    let file = File::open(filename);

    let file = match file {
        Ok(file) => file,
        Err(error) => panic!("Problem opening the file: {:?}", error),
    };
    let reader = BufReader::new(file);

    for line in reader.lines() {
        let line = match line {
            Ok(line) if line.starts_with('#') | line.is_empty() => continue,
            Ok(line) => line,
            Err(_) => break,
        };
    
        println!("{}", line);
        let mut translation = Translation::new();
        let parsed_formula = formulaParser::new()
            .parse(&mut translation, &line)
            .unwrap();
    
        let tree = SolverTree::new(parsed_formula, translation);
        println!("Parsed: {:?}", tree);
        println!("Solving...");
        match tree.solve() {
            Some(solution) => println!("{:?}", solution),
            None => println!("No solution has been found."),
        }
    }
}

#[test]
fn test_parser() {
    use propositional_logic::formulaParser;
    use propositional_solver::{Translation};

    let mut translation = Translation::new();
    assert!(formulaParser::new()
        .parse(&mut translation, "a&bjr&c")
        .is_ok());
    assert!(formulaParser::new()
        .parse(&mut translation, "&a|c")
        .is_err());

    let mut translation = Translation::new();
    let parsed_formula = formulaParser::new()
        .parse(&mut translation, "a&bjr&c")
        .unwrap();
    assert_eq!(&format!("{:?}", parsed_formula), "((1 & 2) & 3)");

    let mut translation = Translation::new();
    let parsed_formula = formulaParser::new()
        .parse(&mut translation, "a&((bjr))&c")
        .unwrap();
    assert_eq!(&format!("{:?}", parsed_formula), "((1 & 2) & 3)");

    let mut translation = Translation::new();
    let parsed_formula = formulaParser::new()
        .parse(&mut translation, "a&((bjr)&c)")
        .unwrap();
    assert_eq!(&format!("{:?}", parsed_formula), "(1 & (2 & 3))");
}

pub mod propositional_solver;

#[test]
fn test_solver() {
    use propositional_logic::formulaParser;
    use propositional_solver::{SolverTree, Translation};

    let mut translation = Translation::new();
    let parsed_formula = formulaParser::new()
        .parse(&mut translation, "a&((bjr)&(c | a))")
        .unwrap();

    let tree = SolverTree::new(parsed_formula, translation);
    println!("{:?}", tree);
}
