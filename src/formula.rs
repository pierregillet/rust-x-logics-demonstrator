use std::fmt::{Debug, Error, Formatter};

pub type NumericalProposition = i32;

#[derive(Clone, PartialEq)]
pub enum Formula {
    Proposition(NumericalProposition),
    BinaryNode(Box<Formula>, BinaryOperator, Box<Formula>),
    UnaryNode(UnaryOperator, Box<Formula>),
}

#[derive(Copy, Clone)]
pub enum Operator {
    UnaryOperator(UnaryOperator),
    BinaryOperator(BinaryOperator),
}

#[derive(Copy, Clone, PartialEq)]
pub enum BinaryOperator {
    And,
    Or,
    Imply,
}

#[derive(Copy, Clone, PartialEq)]
pub enum UnaryOperator {
    Not,
}

impl Debug for Formula {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        use self::Formula::*;
        match self {
            Proposition(n) => write!(fmt, "{:?}", n),
            BinaryNode(ref l, op, ref r) => write!(fmt, "({:?} {:?} {:?})", l, op, r),
            UnaryNode(op, ref r) => write!(fmt, "({:?} {:?})", op, r),
        }
    }
}

impl Debug for BinaryOperator {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        use self::BinaryOperator::*;
        match self {
            And => write!(fmt, "∧"),
            Or => write!(fmt, "∨"),
            Imply => write!(fmt, "→"),
        }
    }
}

impl Debug for UnaryOperator {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
        use self::UnaryOperator::*;
        match self {
            Not => write!(fmt, "¬"),
        }
    }
}
