use std::collections::HashSet;
use std::fmt::{Debug};

use crate::formula::{Formula, NumericalProposition};

type Affectation = HashSet<NumericalProposition>;

#[derive(Debug)]
pub struct SolverTree {
    formula: Formula,
    translation: Translation,
}

impl SolverTree {
    pub fn new(formula: Formula, translation: Translation) -> SolverTree {
        SolverTree {
            formula,
            translation
        }
    }

    pub fn solve(self) -> Option<Affectation> {
        let x = self.prepare_tree();
        println!("{:?}", x);
        return explore_tree(&x, &mut HashSet::new());

        fn explore_tree(tree: & Formula, known_affectations: &mut Affectation) -> Option<Affectation> {
            // The formulas are expected to be in Negation Normal Form for now.
            // May be able to parallelize this recursive function with a threadpool.
            use crate::formula::Formula::*;
            match tree {
                Proposition(prop) => {
                    if known_affectations.contains(&-prop) {
                        Option::None
                    } else {
                        known_affectations.insert(*prop);
                        Some(known_affectations.clone())
                    }
                },
                BinaryNode(l, op, r) => {
                    use crate::formula::BinaryOperator::*;
                    match op {
                        And => {
                            // Keeping the knowledge from the first branch
                            explore_tree(l, known_affectations)
                                .and_then(|mut affectations| explore_tree(r, &mut affectations))
                        },
                        Or => {
                            // Don't add the knowledge from the first branch
                            explore_tree(l, &mut known_affectations.clone())
                                .or_else(|| explore_tree(r, known_affectations))
                        },
                        _ => panic!("Unexpected operator. The formula should be in Negation Normal Form.")
                    }
                },
                UnaryNode(op, r) => {
                    use crate::formula::UnaryOperator::*;
                    match op {
                        Not => {
                            match **r {
                                Proposition(prop) => {
                                    if known_affectations.contains(&prop) {
                                        println!("Can't add: {} ; Knowledge: {:?} ; Current step: {:?}", -prop, known_affectations, tree);
                                        Option::None
                                    } else {
                                        known_affectations.insert(-prop);
                                        Some(known_affectations.clone())
                                    }
                                },
                                _ => panic!("Unexpected leaf, propositions should be integers."),
                            }
                        }
                    }
                },
            }
        }
    }


    fn prepare_tree(self) -> Formula {
        fn explore_tree(tree: & Formula, negative_carry: bool) -> Formula {
            // May be able to parallelize this recursive function with a threadpool.
            use crate::formula::Formula::*;
            use crate::formula::UnaryOperator::Not;
            match tree {
                Proposition(prop) => {
                    let new_prop = Proposition(*prop as NumericalProposition);
                    if negative_carry {
                        UnaryNode(Not, Box::new(new_prop))
                    } else {
                        new_prop
                    }
                },
                BinaryNode(l, op, r) => {
                    use crate::formula::BinaryOperator::*;
                    match op {
                        Imply => {
                            if negative_carry {
                                BinaryNode(
                                    Box::new(explore_tree(l, false)),
                                    And,
                                    Box::new(explore_tree(r, true)),
                                )
                            } else {
                                BinaryNode(
                                    Box::new(explore_tree(l, true)),
                                    Or,
                                    Box::new(explore_tree(r, false)),
                                )
                            }
                        },
                        And => {
                            if negative_carry {
                                BinaryNode(
                                    Box::new(explore_tree(l, true)),
                                    Or,
                                    Box::new(explore_tree(r, true)),
                                )
                            } else {
                                BinaryNode(
                                    Box::new(explore_tree(l, false)),
                                    And,
                                    Box::new(explore_tree(r, false)),
                                )
                            }
                        },
                        Or => {
                            if negative_carry {
                                BinaryNode(
                                    Box::new(explore_tree(l, true)),
                                    And,
                                    Box::new(explore_tree(r, true)),
                                )
                            } else {
                                BinaryNode(
                                    Box::new(explore_tree(l, false)),
                                    Or,
                                    Box::new(explore_tree(r, false)),
                                )
                            }
                        }
                    }
                },
                UnaryNode(op, r) => {
                    match op {
                        Not => {
                            explore_tree(r, !negative_carry)
                        }
                    }
                },
            }
        }
        explore_tree(&self.formula, false)
    }
}

#[derive(Debug)]
pub struct Translation {
    propositions: Vec<String>,
}

impl Translation {
    pub fn new() -> Translation {
        Translation {
            propositions: vec![],
        }
    }

    pub fn get_else_add_translation(&mut self, literal: &String) -> NumericalProposition {
        let literal_position = self.propositions.iter().position(|x| x == literal);
        match literal_position {
            Some(position) => (position + 1) as NumericalProposition,
            None => {
                self.propositions.push(literal.clone());
                return (self.propositions.len() - 1 + 1) as NumericalProposition;
            }
        }
    }
}
